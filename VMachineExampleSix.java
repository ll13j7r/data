import java.io.*;
import java.util.*;
import org.opennebula.client.Client;
import org.opennebula.client.template.Template;
import org.opennebula.client.OneResponse;
import org.opennebula.client.vm.VirtualMachine;
import org.opennebula.client.vm.VirtualMachinePool;

import java.util.concurrent.TimeUnit;

public class VMachineExampleSix{


	/**
	 * Logs into the cloud requesting the user's name and password
	 * @param oneClient
	 * @return
	 */
	public Client logIntoCloud() {

		String passwd;
		Client oneClient = null;
		System.out.println("Enter your password: ");
		String username = System.getProperty("user.name");
		passwd = new String(System.console().readPassword("[%s]", "Password:"));
		try
		{
			oneClient = new Client(username + ":" + passwd, "https://csgate1.leeds.ac.uk:2633/RPC2");
			System.out.println("Authentication successful ...");
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			System.out.println("Incorrect Password. Program Closing.");
			System.exit(1);
		}
		return oneClient;
	}

    
    public String executeCommand(String command) {
                StringBuffer output = new StringBuffer();
                Process p;
                try {

                    p = Runtime.getRuntime().exec(command);
                    p.waitFor();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

                    String line = "";
                    while ((line = reader.readLine())!= null) {
                        output.append(line + "\n");
                    }

                }catch(Exception e){
                    e.printStackTrace();
                }

                return output.toString();

    }


    public static void main(String[] args)
    {

		try
		{
		    //create the VMSample object to complete the coursework
			VMachineExampleSix vmSample = new VMachineExampleSix();
            List <OneResponse> responseList = new ArrayList <OneResponse>();
            List <Template> templateList = new ArrayList <Template>();

		    //log into the cloud and return the client
			Client oneClient = vmSample.logIntoCloud();

          

            // We will try to create a new virtual machine. The first thing we
            // need is an OpenNebula virtual machine template.

            Template master = new Template (303,oneClient);
            Template slave1 = new Template (311,oneClient);
            Template slave2 = new Template (312,oneClient);
            Template slave3 = new Template (313,oneClient);
            Template slave4 = new Template (322,oneClient);
            Template slave5 = new Template (323,oneClient);
            Template slave6 = new Template (324,oneClient);
            Template slave7 = new Template (325,oneClient);


            System.out.print("Trying to instantiate the virtual machine... ");

            OneResponse rc = master.instantiate();
            TimeUnit.MINUTES.sleep(1);
       
            

            if(rc.isError()){
                System.out.println("failed!");
                throw new Exception( rc.getErrorMessage() );
            }else{
                System.out.println("MASTER Instantiated");
            }

            OneResponse rc1 = slave1.instantiate();
            OneResponse rc2 = slave2.instantiate();
            OneResponse rc3 = slave3.instantiate();
            OneResponse rc4 = slave4.instantiate();
            OneResponse rc5 = slave5.instantiate();
            OneResponse rc6 = slave6.instantiate();
            OneResponse rc7 = slave7.instantiate();
        
            responseList.add(rc1);
            responseList.add(rc2);
            responseList.add(rc3);
            responseList.add(rc4);
            responseList.add(rc5);
            responseList.add(rc6);
            responseList.add(rc7);

            for (OneResponse response: responseList){
                if(response.isError()){
                    System.out.println("failed!");
                    throw new Exception( response.getErrorMessage() );
                }else{
                    System.out.println("VM Instantiated");
                }
            }

            TimeUnit.MINUTES.sleep(2);

            VirtualMachinePool vmPool = new VirtualMachinePool(oneClient);
            rc = vmPool.info();

            if(rc.isError())
                throw new Exception( rc.getErrorMessage() );

            System.out.println(
                    "\nThese are all the Virtual Machines in the pool:");
            for ( VirtualMachine vmachine : vmPool )
            {
                System.out.println("\tID :" + vmachine.getId() +
                                   ", Name :" + vmachine.getName() );
            }
            

           


	    System.out.println("Program complete");
        }

        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

}

}
